import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor() { }
  save(session: any) {
    localStorage.setItem('sp-session', JSON.stringify(session));

  }
  get(): any {
    const savedSession = localStorage.getItem('sp-session');
    return savedSession ? JSON.parse(savedSession) : false

  }
}
