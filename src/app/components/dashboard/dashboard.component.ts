import { SessionService } from './../../services/session/session.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor( private session: SessionService ) { }

  ngOnInit(): void {
  }
  get username() {
    return this.session.get().username;
  }

}
